# Relief Site

## Context
This site is meant to be a repository of locations offering aid. Those that are seeking aid, or would like to 
contribute in the relief effort can use the site to fine locations near them 

## Quick Start
If you would like to contribute locations to the list you can update the "website.yaml". 
In the "website.yaml" there is a property "locations" which is an array of locations. Copy one of the items and fill in the relevant data.
If you don't have info for one of the fields you can leave it null by place a tilde (~)
```
-
    title: Some Location Name
    description: Basic instructions for donating here 
    latitude: ~
    longitude: ~
    address1: ~
    address2: ~
    town: ~
    phone1: 123451
    contact1: John Doe
    phone2: ~
    contact2: ~
    email: ~
    website: ~
    facebook: ~
```	

## How it works
When commits are made to the repo a build process begins that generates the website and uploads to a s3 that hosts the site. 